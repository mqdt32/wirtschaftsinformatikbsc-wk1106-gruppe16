package com.gruppe16.supermariorun;

/** ~~~~~~~~~~ IMPORTS ~~~~~~~~~~ */
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;

import java.util.ArrayList;
import java.util.Random;

/**
 * Dropdown Menu:
 *  - (Pause game)
 *  - Restart / Resume
 *  - Background music
 *  - Sounds
 *  - Background
 *  - Game rules
 *      - Bomb dead
 *      - Enemy (subtract life (3))
 *  - Language (EN / DE)
 *
 *  Move background
 *
 *  Memory Management (?)
 */


public class SuperMarioRun extends ApplicationAdapter {
	/** ~~~~~~~~~~ ATTRIBUTES game state ~~~~~~~~~~ */
	SpriteBatch batch;
	Texture background;
	BitmapFont font;
//	Item coinTest, bombTest;
	GameState state;
//	GameState gameStateStart, gameStateRun, gameStateEnd;
//	int gameStateCounter = 0;
//	int score = 0;


	/** ~~~~~~~~~~ ATTRIBUTES man ~~~~~~~~~~ */
//	Texture[] standingMan;
//	Texture[] runningMan;
//	Texture[] dizzyMan;
//	Random random;
//	Rectangle manRectangle;
//	float gravity = 0.2f;
//	float velocity = 0;
//	float manY;
//	int standingState = 0;
//	int runningState = 0;
//	int dizzyState = 0;
//	int pause = 0;

	/** ~~~~~~~~~~ ATTRIBUTES coin ~~~~~~~~~~ */
//	Texture coin;
//	ArrayList<Integer> coinXs = new ArrayList<>();
//	ArrayList<Integer> coinYs = new ArrayList<>();
//	ArrayList<Rectangle> coinRectangle = new ArrayList<>();
//	int coinCount;

	/** ~~~~~~~~~~ ATTRIBUTES bomb ~~~~~~~~~~ */
//	Texture bomb;
//	ArrayList<Integer> bombXs = new ArrayList<>();
//	ArrayList<Integer> bombYs = new ArrayList<>();
//	ArrayList<Rectangle> bombRectangle = new ArrayList<>();
//	int bombCount;

	/** ~~~~~~~~~~ METHOD create() ~~~~~~~~~~
	 * 	Initializes the variables required to generate the playing field.
	 * 		- draws background (https://opengameart.org/content/bevouliin-free-mountain-game-background-for-game-developers)
	 * 		- draws running man (https://opengameart.org/content/bevouliin-free-running-and-jumping-mascot-sprite-sheets)
	 * 		- draws coins (https://www.flaticon.com/free-icon/coin_138233?term=coin&page=1&position=15&page=1&position=15&related_id=138233&origin=search)
	 * 		- draws bombs (https://www.flaticon.com/free-icon/visual-effect_4144639?term=bomb&page=1&position=45&page=1&position=45&related_id=4144639&origin=search)
	 */
	@Override
	public void create () {
		batch = new SpriteBatch();
		background = new Texture("background.png");
//		random = new Random();

		state = new GameStateStart(batch);
//		gameStateRun = new GameStateRun(batch);

//		manY = Gdx.graphics.getHeight() / 2.0f;

		// Initializes the score count
//		font = new BitmapFont();
//		font.setColor(Color.WHITE);
//		font.getData().setScale(10);
	}

	int pause_test = 0;

	/** ~~~~~~~~~~ METHOD render() ~~~~~~~~~~
	 *
	 */
	@Override
	public void render () {
		batch.begin();
		batch.draw(background, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

		if (pause_test < 5) {
			pause_test++;
		} else {
			pause_test = 0;
		}

		switch (state.getState()) {

			case ONE:

				state = state.gamePlay();

			case TWO:

				state =state.gamePlay();

			case THREE:

				state = state.gamePlay();

		}




//		if (gameStateCounter == 0) {
//			/** GAME READY TO GO */
//			if (Gdx.input.justTouched()) {
//				gameStateCounter = 1;
//			}
//			gameStateStart.drawMan();
//		}
//		else if (gameStateCounter == 1) {
//			/** GAME IS RUNNING */
//
//			manRectangle = new Rectangle(Gdx.graphics.getWidth() / 2.0f - runningMan[0].getWidth() / 2.0f, manY, runningMan[runningState].getWidth(), runningMan[runningState].getHeight());
//			for (int i = 0; i < coinRectangle.size(); i++) {
//				if (Intersector.overlaps(manRectangle, coinRectangle.get(i))) {
//					Gdx.app.log("COIN", "Coin collides with player");
//					score++;
//					coinRectangle.remove(i);
//					coinXs.remove(i);
//					coinYs.remove(i);
//					break;
//				}
//			}
//			for (int i = 0; i < bombRectangle.size(); i++) {
//				if (Intersector.overlaps(manRectangle, bombRectangle.get(i))) {
//					Gdx.app.log("BOMB", "Bomb collides with player");
//					gameStateCounter = 2;
//				}
//			}
//			batch.draw(runningMan[runningState], Gdx.graphics.getWidth() / 2.0f - runningMan[0].getWidth() / 2.0f, manY);
//		}
//		else if (gameStateCounter == 2) {
//			/** GAME OVER */
//			if (pause < 5) {
//				pause++;
//			} else {
//				pause = 0;
//				if (dizzyState < 1) {
//					dizzyState++;
//				} else {
//					dizzyState = 0;
//				}
//			}
//			batch.draw(dizzyMan[dizzyState], Gdx.graphics.getWidth() / 2.0f - dizzyMan[0].getWidth() / 2.0f, manY);
//			if (Gdx.input.justTouched()) {
//				score = 0;
//				gameStateCounter = 1;
//				velocity = 0;
//				coinXs.clear();
//				coinYs.clear();
//				coinRectangle.clear();
//				coinCount = 0;
//				bombXs.clear();
//				bombYs.clear();
//				bombRectangle.clear();
//				bombCount = 0;
//				manY = Gdx.graphics.getHeight() / 2.0f;
//			}
//		}
//
//		font.draw(batch, String.valueOf(score), 100, 200);
		batch.end();
	}

	/** ~~~~~~~~~~ METHOD dispose() ~~~~~~~~~~
	 *
	 */
	@Override
	public void dispose () {
		batch.dispose();
	}

}
