package com.gruppe16.supermariorun;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class GameStateEnd extends GameState {

    SpriteBatch batch;

    private Texture[] animation = new Texture[2];
    private int animationState;
    private int pause = 0;
    public float manY = Gdx.graphics.getHeight() / 2.0f;

    /**
     *
     * @param batch
     */
    public GameStateEnd(SpriteBatch batch) {

        this.batch = batch;

        animation[0] = new Texture("dizzy-1.png");
        animation[1] = new Texture("dizzy-2.png");
        animationState = 2;

    }

    @Override
    public State getState() {
        return State.THREE;
    }

    @Override
    public GameState gamePlay() {

        Gdx.app.log("STATE", "Current State is: End.");

        if (Gdx.input.justTouched()) {
            return new GameStateStart(batch);
        }
        drawMan();
        return this;
    }

    @Override
    public void drawMan() {

        if (pause < 5) {
            if (animationState < 1) {
                animationState++;
            } else {
                animationState = 0;
            }
        }
        batch.draw(animation[animationState], Gdx.graphics.getWidth() / 2.0f - animation[0].getWidth() / 2.0f, manY);
    }


}
