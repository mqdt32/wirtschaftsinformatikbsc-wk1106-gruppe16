package com.gruppe16.supermariorun;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class GameStateRun extends GameState {

    SpriteBatch batch;

    private Texture[] animation = new Texture[5];
    private int animationState;
    private int pause = 0;
    public float manY = Gdx.graphics.getHeight() / 2.0f;

    private Item coin;
    private int coinCount = 0;
    private int coinAnimationSpeed = 150;

    private Item bomb;
    private int bombCount = 0;
    private int bombAnimationSpeed = 300;

    private BitmapFont font;
    private int score = 0;

    /**
     *
     * @param batch
     */
    public GameStateRun(SpriteBatch batch) {

        this.batch = batch;

        animation[0] = new Texture("run-1.png");
        animation[1] = new Texture("run-2.png");
        animation[2] = new Texture("run-3.png");
        animation[3] = new Texture("run-4.png");
        animation[4] = new Texture("run-5.png");
        animationState = 5;

        coin = new Coin(batch);
        bomb = new Bomb(batch);

        font = new BitmapFont();
        font.setColor(Color.WHITE);
        font.getData().setScale(10);

    }


    @Override
    public State getState() {
        return State.TWO;
    }

    @Override
    public void drawMan() {

        if (pause < 5) {
            if (animationState < 4) {
                animationState++;
            } else {
                animationState = 0;
            }
        }
        batch.draw(animation[animationState], Gdx.graphics.getWidth() / 2.0f - animation[0].getWidth() / 2.0f, manY);
    }

    public GameState gamePlay() {

        Gdx.app.log("STATE", "Current State is: Run.");

        // Generate coins
        if (coinCount < coinAnimationSpeed) {
            coinCount++;
        } else {
            coinCount = 0;
            coin.generateItem();
        }

        // Generate bombs
        if (bombCount < bombAnimationSpeed) {
            bombCount++;
        } else {
            bombCount = 0;
            bomb.generateItem();
        }

        // Draw coins and bombs
        coin.drawItem();
        bomb.drawItem();

        // Check if jumped
        manY = man.jump(manY);

        if (man.checkCollision(coin.itemRectangle)) {
            score++;
        }

        if (man.checkCollision(bomb.itemRectangle)) {
            return new GameStateEnd(batch);
        }

        // Draw score and man
        drawMan();
        font.draw(batch, String.valueOf(score), 100, 200);


        return this;
    }


}
