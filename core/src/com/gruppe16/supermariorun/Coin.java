package com.gruppe16.supermariorun;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public class Coin extends Item {

    SpriteBatch batch;

    Texture coin;

    public Coin(SpriteBatch batch) {

        this.batch = batch;

        coin = new Texture("coin.png"); // Coin size 150 x 150

    }

    @Override
    public void generateItem() {
        super.generateItem();
    }

    public void drawItem() {

        itemRectangle.clear();
        for (int i = 0; i < itemXs.size(); i++) {

            batch.draw(coin, itemXs.get(i), itemYs.get(i));
            itemXs.set(i, itemXs.get(i) - movementSpeed);
            itemRectangle.add(new Rectangle(itemXs.get(i), itemYs.get(i), coin.getWidth(), coin.getHeight()));

        }

    }

}
