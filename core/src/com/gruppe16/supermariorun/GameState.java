package com.gruppe16.supermariorun;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

public abstract class GameState {



    enum State {
        ONE,
        TWO,
        THREE
    }

    Man man = new Man();

    public abstract State getState();
    public abstract GameState gamePlay();
    public abstract void drawMan();

}
