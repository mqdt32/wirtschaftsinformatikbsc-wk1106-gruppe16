package com.gruppe16.supermariorun;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Rectangle;

import java.util.ArrayList;
import java.util.Random;

public abstract class Item {

    ArrayList<Integer> itemXs = new ArrayList<>();
    ArrayList<Integer> itemYs = new ArrayList<>();
    ArrayList<Rectangle> itemRectangle = new ArrayList<>();
    Random random = null;
    int movementSpeed = 4;

    public void generateItem() {

        float height = random.nextFloat() * Gdx.graphics.getHeight();
        itemYs.add((int) height);
        itemXs.add(Gdx.graphics.getWidth() - movementSpeed);

    }

    public abstract void drawItem();
}
