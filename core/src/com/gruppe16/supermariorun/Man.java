package com.gruppe16.supermariorun;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;

import java.util.ArrayList;

public class Man {

    Rectangle manRectangle;

    private int velocity = 0;
    private final int jumpHeight = 10;
    private final float gravity = 0.2f;

    public Man() {

        manRectangle = null;

    }

    /**
     * Checks whether the man has collided with an item.
     *
     * @param item List of items to check for collision.
     * @return True if the man collided.
     */
    public boolean checkCollision (ArrayList<Rectangle> item) {
        for (int i = 0; i < item.size(); i++) {
            if (Intersector.overlaps(manRectangle, item.get(i))) {
                Gdx.app.log("COLLISION", "Player collided with a item");
                return true;
            }
        }
        return false;
    }

    public float jump(float cur_manY) {

        if (Gdx.input.justTouched()) {

            velocity = -10;
            // TODO: Create a variable for the jump height

        }

        velocity += gravity;
        float new_manY = cur_manY - velocity;

        if (new_manY <= 0) {

            new_manY = 0;
            // TODO: Currently jumps out of the field.

        }

        return new_manY;

    }

}
