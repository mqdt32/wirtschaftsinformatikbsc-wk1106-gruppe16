package com.gruppe16.supermariorun;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public class Bomb extends Item {

    SpriteBatch batch;

    Texture bomb;

    public Bomb(SpriteBatch batch) {

        this.batch = batch;

        bomb = new Texture("bomb.png"); // Bomb size 150 x 150 TODO maybe a little smaller?

    }

    public void drawItem() {

        itemRectangle.clear();
        for (int i = 0; i < itemXs.size(); i++) {

            batch.draw(bomb, itemXs.get(i), itemYs.get(i));
            itemXs.set(i, itemXs.get(i) - movementSpeed);
            itemRectangle.add(new Rectangle(itemXs.get(i), itemYs.get(i), bomb.getWidth(), bomb.getHeight()));

        }

    }


}
